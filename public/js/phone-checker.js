var phoneLocal;

function validatePhoneOnClick() {
    var $phoneCheckInput = $('#phoneCheckInput');
    var phone = $phoneCheckInput.val();
    var $countryLabel = $('#countryLabel');
    var validPhone = validatePhoneKeyDown();
    if (!validPhone) {
        return;
    }
    phoneLocal = phone;
    $.ajax({
        url: "http://localhost:8080/check-phone",
        data: {phone: phone},
        success: function (data, text) {
            if (data) {
                $countryLabel.html(data);
            } else {
                $countryLabel.html("Country not found.");
            }
        },
        error: function (request, status, error) {
            alert(request.responseText);
        }
    });
}
function validatePhoneKeyDown() {
    var phoneValid = validatePhoneInput();
    $('#validatePhoneButton').attr("disabled", !phoneValid);
    $('#countryLabel').html("");
    return phoneValid
}
function validatePhoneInput() {
    var $phoneCheckInput = $('#phoneCheckInput');
    var phone = $phoneCheckInput.val();
    phone = phone.replace(/\D/g, '');
    return (phone.length >= 7 && phone.length <= 15);
}
$("#phoneCheckInput").blur(function () {
    var phoneInput = $('#phoneCheckInput').val();
    if (phoneLocal !== undefined && phoneLocal != null &&  phoneLocal == phoneInput) {
        return;
    }
    validatePhoneKeyDown();
});