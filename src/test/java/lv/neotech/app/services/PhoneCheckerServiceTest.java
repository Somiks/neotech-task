package lv.neotech.app.services;

import lv.neotech.app.components.CountryCallingPhonesLoaderCached;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class PhoneCheckerServiceTest {
    @InjectMocks
    private PhoneCheckerService phoneCheckerService;
    @Mock
    private CountryCallingPhonesLoaderCached countryCallingPhonesLoaderCache;
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getCountryByPhone() {
        verifyNoMoreInteractions(countryCallingPhonesLoaderCache);

        final Map<Integer, String> countryCallingList = new HashMap<>();
        countryCallingList.put(371,"Latvia");
        countryCallingList.put(7, "Russia");
        when(countryCallingPhonesLoaderCache.getCountryCallingPhones()).thenReturn(countryCallingList);
        final String countryByPhoneCode = phoneCheckerService.getCountryByPhoneCode("3712247521668");
        assertEquals("Latvia", countryByPhoneCode);
    }

    @Test
    public void countryNotFound() {
        verifyNoMoreInteractions(countryCallingPhonesLoaderCache);

        final Map<Integer, String> countryCallingList = new HashMap<>();
        countryCallingList.put(371,"Latvia");
        countryCallingList.put(7, "Russia");
        when(countryCallingPhonesLoaderCache.getCountryCallingPhones()).thenReturn(countryCallingList);
        final String countryByPhoneCode = phoneCheckerService.getCountryByPhoneCode("3722247521668");
        assertNull(countryByPhoneCode);
    }

    @Test
    public void invalidIncomePhone() {
        verifyNoMoreInteractions(countryCallingPhonesLoaderCache);

        final Map<Integer, String> countryCallingList = new HashMap<>();
        countryCallingList.put(371,"Latvia");
        countryCallingList.put(7, "Russia");
        when(countryCallingPhonesLoaderCache.getCountryCallingPhones()).thenReturn(countryCallingList);
        thrown.expect(RuntimeException.class);
        thrown.expectMessage("Lost phone number");
        final String countryByPhoneCode = phoneCheckerService.getCountryByPhoneCode(" ");
    }

    @Test
    public void shortIncomePhone() {
        verifyNoMoreInteractions(countryCallingPhonesLoaderCache);

        final Map<Integer, String> countryCallingList = new HashMap<>();
        countryCallingList.put(371,"Latvia");
        countryCallingList.put(7, "Russia");
        when(countryCallingPhonesLoaderCache.getCountryCallingPhones()).thenReturn(countryCallingList);
        thrown.expect(RuntimeException.class);
        thrown.expectMessage("Phone number too short or too long");
        final String countryByPhoneCode = phoneCheckerService.getCountryByPhoneCode("02");
    }


    @Test
    public void longIncomePhone() {
        verifyNoMoreInteractions(countryCallingPhonesLoaderCache);

        final Map<Integer, String> countryCallingList = new HashMap<>();
        countryCallingList.put(371,"Latvia");
        countryCallingList.put(7, "Russia");
        when(countryCallingPhonesLoaderCache.getCountryCallingPhones()).thenReturn(countryCallingList);
        thrown.expect(RuntimeException.class);
        thrown.expectMessage("Phone number too short or too long");
        final String countryByPhoneCode = phoneCheckerService.getCountryByPhoneCode("1354168138513134");
    }

}