package lv.neotech.app.components;

import lv.neotech.app.services.WebPageLoaderService;
import lv.neotech.app.utils.PhoneCheckerParser;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;


public class CountryCallingPhonesLoaderCachedTest {
    private static String COUNTRY_CALLING_CODES_URL = "https://en.wikipedia.org/wiki/List_of_country_calling_codes";

    @InjectMocks
    private CountryCallingPhonesLoaderCached countryCallingPhonesLoaderCache;
    @Mock
    private WebPageLoaderService webPageLoaderService;
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getCountryCallingPhones() throws Exception {
        verifyNoMoreInteractions(webPageLoaderService);

        when(webPageLoaderService.getHtmlPage(COUNTRY_CALLING_CODES_URL)).thenReturn(LongCountryCallingPhoneTestString.COUNTRY_CALLING_PHONES_WEB);
        final Map<Integer, String> countryCallingPhones = countryCallingPhonesLoaderCache.getCountryCallingPhones();

        assertEquals(296, countryCallingPhones.size());
        assertEquals("Latvia", countryCallingPhones.get(371));
        assertEquals("Mayotte", countryCallingPhones.get(262639));
        assertEquals("United States", countryCallingPhones.get(1));
        assertEquals("Russia", countryCallingPhones.get(7));
    }

    @Test
    public void listChanged() throws Exception {
        verifyNoMoreInteractions(webPageLoaderService);

        when(webPageLoaderService.getHtmlPage(COUNTRY_CALLING_CODES_URL)).thenReturn(LongCountryCallingPhoneTestString.COUNTRY_CALLING_PHONES_INVALID_COUNT);
        thrown.expect(RuntimeException.class);
        thrown.expectMessage("Phone country codes table is changed");
        countryCallingPhonesLoaderCache.getCountryCallingPhones();
    }

    @Test
    public void pageIsEmpty() throws Exception {
        verifyNoMoreInteractions(webPageLoaderService);

        when(webPageLoaderService.getHtmlPage(COUNTRY_CALLING_CODES_URL)).thenReturn("");
        thrown.expect(RuntimeException.class);
        thrown.expectMessage("html is null");
        countryCallingPhonesLoaderCache.getCountryCallingPhones();
    }
    //...
}