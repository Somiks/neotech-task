package lv.neotech.app.controllers;


import lv.neotech.app.services.PhoneCheckerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class PhoneCheckerController {

    @Autowired
    private PhoneCheckerService phoneCheckerService;

    @GetMapping("/check-phone")
    public String checkPhone(@RequestParam String phone) {
        return phoneCheckerService.getCountryByPhoneCode(phone);
    }
}
