package lv.neotech.app;

import lv.neotech.app.components.CountryCallingPhonesLoaderCached;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableCaching
public class Application {

    @Autowired
    private CountryCallingPhonesLoaderCached countryCallingPhonesLoaderCache;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }



    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {
            countryCallingPhonesLoaderCache.getCountryCallingPhones();
        };
    }

}