package lv.neotech.app.utils;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


public class PhoneCheckerParser {
    public static Map<Integer, String> getCountryPhonesList(String html) {
        if (StringUtils.isBlank(html)) {
            throw new RuntimeException("html is null");
        }
        Document doc = Jsoup.parse(html);
        Elements tablesElement = doc.select("table");
        Elements phoneCodeTableRows = getPhoneCodeTableRows(tablesElement);
        Map<Integer, String> countriesByPhone = new HashMap<>();
        handleRows(phoneCodeTableRows, countriesByPhone);
        return Collections.unmodifiableMap(countriesByPhone);
    }

    private static void handleRows(Elements phoneCodeTableRows,
                            Map<Integer, String> countriesByPhone) {
        for (int i = 1; i < phoneCodeTableRows.size(); i++) {
            Element row = phoneCodeTableRows.get(i);
            Elements cols = row.select("td");
            Element countryColumn = cols.get(0);

            if (countryColumn == null) {
                throw new RuntimeException("Country column not found for row - " + i);
            }
            String country = countryColumn.text();
            Element phoneColumn = cols.get(1);
            if (phoneColumn == null) {
                throw new RuntimeException("Phones column not found for country - " + country);
            }
            Elements links = phoneColumn.select("a");
            if (links.size() == 0) {
                throw new RuntimeException("Phone column doesn't contain any phone for country - " + country);
            }
            handlePhoneLinks(countriesByPhone, country, links);
        }
    }

    private static  void handlePhoneLinks(Map<Integer, String> countriesByPhone,
                                          String country, Elements links) {
        for (Element link : links) {
            String phoneFromLink = link.text();
            appendPhoneAndCountries(countriesByPhone, country, phoneFromLink);
        }
    }

    private static void appendPhoneAndCountries(
            Map<Integer, String> countriesByPhone, String country,
            String phoneFromLink) {

        if (phoneFromLink != null && phoneFromLink.startsWith("+")) {
            if (phoneFromLink.contains(",")) {
                String[] multiplePhonesForOneLink = phoneFromLink.split(",");
                for (String phone : multiplePhonesForOneLink) {
                    Integer rawPhoneNr = getRawPhoneNr(phone);
                    if (rawPhoneNr > 5)
                        countriesByPhone.put(rawPhoneNr, country);
                }
            } else {
                Integer rawPhoneNr = getRawPhoneNr(phoneFromLink);
                countriesByPhone.put(rawPhoneNr, country);
            }
        }
    }

    private static Integer getRawPhoneNr(String linkData) {
        return Integer.valueOf(linkData.replaceAll("[+\\s+]", ""));
    }

    private static Elements getPhoneCodeTableRows(Elements tablesElement) {
        if (tablesElement == null) {
            throw new RuntimeException("Not found any table in the page");
        }
        for (Element table : tablesElement) {
            Elements rows = table.select("tr");
            if (rows.size() == 282) {
                return rows;
            }
        }
        throw new RuntimeException("Phone country codes table is changed");
    }

}
