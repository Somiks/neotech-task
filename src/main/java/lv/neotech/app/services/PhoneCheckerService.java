package lv.neotech.app.services;

import lv.neotech.app.components.CountryCallingPhonesLoaderCached;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service
public class PhoneCheckerService {
    public static final int MAX_LENGTH_OF_COUNTRY_CODE = 7;
    @Autowired
    private CountryCallingPhonesLoaderCached countryCallingPhonesLoaderCache;

    public String getCountryByPhoneCode(String phone) {
        if (StringUtils.isBlank(phone)) {
            throw new IllegalArgumentException("Lost phone number");
        }
        phone = phone.replaceAll("[^\\d.]", "");
        if (phone.length() < 7 || phone.length() > 15) {
            throw new IllegalArgumentException("Phone number too short or too long");
        }
        final Map<Integer, String> countryCallingPhones = countryCallingPhonesLoaderCache.getCountryCallingPhones();
        for(int i = MAX_LENGTH_OF_COUNTRY_CODE; i > 0; i--) {
            final Integer phoneInt = Integer.valueOf(phone.substring(0, i));
            final String country = countryCallingPhones.get(phoneInt);
            if (country != null) {
                return country;
            }
        }
        return null;
    }
}
