package lv.neotech.app.services;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;

@Service
public class WebPageLoaderService {
    public String getHtmlPage(String url) {
        try(InputStream in = new URL(url).openStream()) {
            return IOUtils.toString(in, Charset.forName("UTF-8"));
        } catch (MalformedURLException e) {
            throw new RuntimeException("Invalid URL",e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
