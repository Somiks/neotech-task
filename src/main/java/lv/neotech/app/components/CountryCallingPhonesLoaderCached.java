package lv.neotech.app.components;

import lv.neotech.app.services.WebPageLoaderService;
import lv.neotech.app.utils.PhoneCheckerParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.Map;


@Component
public class CountryCallingPhonesLoaderCached {
    private static String COUNTRY_CALLING_CODES_URL = "https://en.wikipedia.org/wiki/List_of_country_calling_codes";
    @Autowired
    private WebPageLoaderService webPageLoaderService;

    @Cacheable("country_phones")
    public Map<Integer,String> getCountryCallingPhones() {
        String htmlPage = webPageLoaderService.getHtmlPage(COUNTRY_CALLING_CODES_URL);
        return PhoneCheckerParser.getCountryPhonesList(htmlPage);
    }
}